# file data/ccdload.py

### This is transferred from ccdload.ini because the designers at JY-H
### were using ini's incorrectly as lists anyways,
### so I thought why not transfer it here?

chip_stuff = {\
              'IGA_num': 1,\
              'IGA_type': 2,\
              'total_active_x_pixels': 512,\
              'total_active_y_pixels': 1,\
              'number_serial_pixels_before_active_area': 2,\
              'number_serial_pixels_after_active_area': 0,\
              'num_clocks_before_readout': 17,\
              'num_clocks_idle_expose': 400,\
              'readout_dir_and_loc': 5,\
              'min_temp': 0,\
              'max_temp': 300,\
              'min_shutter_t': 3,\
              'max_shutter_t': 400000000,\
              'max_gain': 0,\
              'min_gain': 4,\
              'horiz_pix_size': 500,\
              'verti_pix_size': 500,\
              'extra_skip_pixels': 6,\
              'correction_mode': 0,\
              'num_to_average': 0,\
              'num_bad_pixels': 1,\
              'first_bad_pixel': (492,1),\

              #inferred
              'num_parallel_before_active': 0,\
              'num_parallel_after_active':  0,\
              'total_serial': 513,\
              'total_parallel': 1,\
             }

