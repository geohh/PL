### Abstract some of the common tasks accross GPIB devices in PL experiments.
class AbstractGPIBDevice:
    def __init__(self, address, rm):
        self.resource(rm.open_resource(address))
        self.instrument_address = address

        self.query = self.resource.query

    def write_raw(self, data_byte):
        self.resource.write_binary_values('',[data_byte])


### Abstract class for some common tasks in Horiba IEEE488 devices
class Spex488Mixin:

    def reboot_if_hung(self):
        """ Signal a Spex488-compatible device to reboot if it is hung (waiting for specific input or otherwise).

        Paramaters
        ----------
        self : Instance of a concrete subclass of Spex488Mixin

        Returns
        -------
        Nothing

        Example
        -------
        >>> import visa
        >>> from libPL.Monochromator import Triax320
        >>> rm = visa.ResourceManager()
        >>> t = Triax320(rm, "GPIB::0::INSTR")
        >>> t.get_status()
        VisaIOError
        >>> t.reboot_if_hung
        >>> t.get_status()
        "B"

        """

        self.resource.write_binary_values('',[222])

    def get_status(self):
        """ Signals a SPEX488-compatible device to return the internal program that it is currently running.

        Paramaters
        ----------
        self : instance of concrete subclass of Spex488Mixin

        Returns
        -------
        status : either "B" -- the "Boot" program, or "F", the "Main" program

        """

        return self.resource.query(" ")

    @property
    def firmware_version(self):
        """Signals a SPEX488-compatible device to return the firmware version it is running,
        and parses the returned string into a float containing version.
        Naturally, is read-only.

        Paramaters
        ----------
        self : instance of a concrete subclass of Spex488Mixin

        Returns
        -------
        ver : float, containing running firmware version

        Example
        -------
        >>> t = Triax320(rm, "GPIB0::0::INSTR")
        >>> t.firmware_version
        3.3

        """

        response = self.resource.query("z")
        if response[0] != 'o':
            raise MonochromatorUnexpectedStatusReturnException("Failed to read firmware version: instead got: {0}".format(response))
        return float(response[2:6])


    def setup(self):
        """
        Generic method for the shared first step of initializing a SPEX488 device.

        Paramaters
        ----------
        self : an instance of a concrete subclass of SPEX488Mixin

        Returns
        -------
        nothing

        Example
        -------
        >>> class MySpectrometer(AbstractGPIBDevice, Spex488Mixin):
        ...     def motor_init(self):
        ...         self.resource.write("A")
        ...     def setup(self):
        ...         super.setup(self)
        ...         self.motor_init
        ...

        """
        status = self.get_status()
        while status != "F":
            if status == "B":
                self.resource.query("O2000\0\r")
            else:
                self.reboot_if_hung()
            time.sleep(5000)


    def full_reboot(self):
        """ Intentionally hang a SPEX488 device so that `reboot_if_hung` will reset it to known state,
        then call setup.
        Paramaters
        ----------
        self : Instance of a concrete subclass of Spex488Mixin

        Returns
        -------
        Nothing

        Example
        -------
        >>> import visa
        >>> from libPL.Monochromator import Triax320
        >>> rm = visa.ResourceManager()
        >>> t = Triax320(rm, "GPIB::0::INSTR")
        >>> t.get_status()
        VisaIOError
        >>> t.full_reboot
        >>> t.get_status()
        "F"

        """

        self.resource.write("H")

        self.reboot_if_hung()
        self.setup()





### Class for storing data from PL experiments
### Contains fields for most necessary information about an experiment
### Initialize calls exec()! Do not use on unsanitized data!
class PLdata:
    __fields = ["comment", "grating", "slit", "shutter_time", "avgs", "pump_info", "objective", "data"]
    def __init__(self, **kwargs):
        for field in PLdata.__fields:
            try:
                exec("self.{0} = kwargs[\"{0}\"]".format(field))
            except IndexError:
                exec("self.{0} = None".format(field))

