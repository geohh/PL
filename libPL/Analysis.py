### Utility functions for PL analysis

from itertools import chain
import numpy as np
from math import factorial
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

from .Patterns import PLdata

def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    r"""Smooth (and optionally differentiate) data with a Savitzky-Golay filter.
    The Savitzky-Golay filter removes high frequency noise from data.
    It has the advantage of preserving the original shape and
    features of the signal better than other types of filtering
    approaches, such as moving averages techniques.
    Parameters
    ----------
    y : array_like, shape (N,)
        the values of the time history of the signal.
    window_size : int
        the length of the window. Must be an odd integer number.
    order : int
        the order of the polynomial used in the filtering.
        Must be less then `window_size` - 1.
    deriv: int
        the order of the derivative to compute (default = 0 means only smoothing)
    Returns
    -------
    ys : ndarray, shape (N)
        the smoothed signal (or it's n-th derivative).
    Notes
    -----
    The Savitzky-Golay is a type of low-pass filter, particularly
    suited for smoothing noisy data. The main idea behind this
    approach is to make for each point a least-square fit with a
    polynomial of high order over a odd-sized window centered at
    the point.
    Examples
    --------
    >>> t = np.linspace(-4, 4, 500)
    >>> y = np.exp( -t**2 ) + np.random.normal(0, 0.05, t.shape)
    >>> ysg = savitzky_golay(y, window_size=31, order=4)
    >>> import matplotlib.pyplot as plt
    >>> plt.plot(t, y, label='Noisy signal')
    >>> plt.plot(t, np.exp(-t**2), 'k', lw=1.5, label='Original signal')
    >>> plt.plot(t, ysg, 'r', label='Filtered signal')
    >>> plt.legend()
    >>> plt.show()

    References
    ----------
    .. [1] A. Savitzky, M. J. E. Golay, Smoothing and Differentiation of
       Data by Simplified Least Squares Procedures. Analytical
       Chemistry, 1964, 36 (8), pp 1627-1639.
    .. [2] Numerical Recipes 3rd Edition: The Art of Scientific Computing
       W.H. Press, S.A. Teukolsky, W.T. Vetterling, B.P. Flannery
       Cambridge University Press ISBN-13: 9780521880688
    """
    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order+1)
    half_window = (window_size -1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve( m[::-1], y, mode='valid')

def pl_smooth(data, windowsize, order):
    r"""Non-destructively generates a new PLdata object with a Savitsky-Golay
    filter smoothing on the actual data, and all other fields copied.
    :param data : a PLdata object containing data to be smoothed
    :param windowsize : int, the size of the smoothing window passed to S-G
    :param order : int, the power of the polynomials the smoothing will be made
    of (must be < windowsize - 1)
    :returns a new PLdata object with all paramaters except for data the same
    """
    x = data.data[:,0]
    y = data.data[:,1]
    yprime = savitzky_golay(y, windowsize, order)
    newdata = np.hstack(x, yprime)
    new = PLdata(comment=data.comment, grating=data.grating, slit=data.slit, shutter_time=data.shutter_time, avgs=data.avgs, pump_info=data.pump_info, objective=data.objective, data=newdata)
    return new

def pl_quick_visualize(data_container, autosmooth=True):
    r"""
    Function to quickly construct a standard PL plot in pyplot.
    Paramaters
    -----------
    autosmooth : if True, will automatically apply a Savitzky-Golay
        low pass filter to the data to smooth it, with paramaters that have worked well
        for me in the past. If those paramaters don't work well for you, or you just don't
        want to smooth, set to False and apply any filters you want manually.
    data_container : a PLdata object containing data to be visualized

    Returns
    ---------
    fig : a PyPlot figure
    """
    if autosmooth:
        data_container = pl_smooth(data_container, len(data_container.data[0]) // 20, 4)

    fig = plt.figure()
    fig.title(data_container.comment)
    fig.ylabel("Intensity (a.u.)")
    fig.xlabel("Wavelength (nm)")

    fig.plot(data_container.data[:,0], data_container.data[:,1])

    return fig


def n_modal(n):
    """Return a function that evaluates a sum of n independant gaussians, np-data compatible.

    Paramaters
    ---------
    n : int, the number of gaussians to sum in the returned function

    Returns
    -------
    multigauss_model : a function of x, mu_0, sigma_0, A_0, mu_1, sigma_1, ... mu_{n-1}, sigma_{n-1}, A_{n-1}
        that returns the sum of those gaussians at x (x can be scalar or vector)

    Example
    -------
    >>> import numpy as np
    >>> from scipy.optimize import curve_fit
    >>> bimodal = n_modal(2)
    >>> xx = np.arange(0, 10, 0.001)
    >>> yy_noisy = bimodal(xx, 3, 1, 10, 8, 0.2, 3) + np.random.normal(1, 1, 1000)
    >>> expected = (4, 5, 6, 7, 8, 9) #Providing reasonably close expected means is somewhat important to NL-LSTSQ fitting multimodals, but the stdevs and heights can easily be corrected by the regression
    >>> params, cov = curve_fit(bimodal, xx, yy_noisy)
    >>> sigma = np.sqrt(np.diag(cov))

    Notes
    -----
    Returned function uses `*args`, so python can't determine staticly
    that it takes 3*n+1 arguments.
    This means that any n_modal function must be `curve_fit`-ed
    with an appropriate number of expected paramaters.


    """
    def gauss(x, mu, sigma, A):
        return A*np.exp(-(x-mu)**2/(2.*sigma**2))
    def multigauss_model(x, *p):
        acc = np.zeros_like(x)
        for i in range(n):
            acc += gauss(x, *(p[3*i:3*(i+1)]))
        return acc
    return multigauss_model




def detect_peaks(x, mph=None, mpd=1, threshold=0, edge='rising',
                 kpsh=False, valley=False, show=False, ax=None):

    """Detect peaks in data based on their amplitude and other features.

    Parameters
    ----------
    x : 1D array_like
        data.
    mph : {None, number}, optional (default = None)
        detect peaks that are greater than minimum peak height.
    mpd : positive integer, optional (default = 1)
        detect peaks that are at least separated by minimum peak distance (in
        number of data).
    threshold : positive number, optional (default = 0)
        detect peaks (valleys) that are greater (smaller) than `threshold`
        in relation to their immediate neighbors.
    edge : {None, 'rising', 'falling', 'both'}, optional (default = 'rising')
        for a flat peak, keep only the rising edge ('rising'), only the
        falling edge ('falling'), both edges ('both'), or don't detect a
        flat peak (None).
    kpsh : bool, optional (default = False)
        keep peaks with same height even if they are closer than `mpd`.
    valley : bool, optional (default = False)
        if True (1), detect valleys (local minima) instead of peaks.
    show : bool, optional (default = False)
        if True (1), plot data in matplotlib figure.
    ax : a matplotlib.axes.Axes instance, optional (default = None).

    Returns
    -------
    ind : 1D array_like
        indeces of the peaks in `x`.

    Notes
    -----
    The detection of valleys instead of peaks is performed internally by simply
    negating the data: `ind_valleys = detect_peaks(-x)`

    The function can handle NaN'

    See this IPython Notebook [1]_.

    References
    ----------
    .. [1] http://nbviewer.ipython.org/github/demotu/BMC/blob/master/notebooks/DetectPeaks.ipynb

    Examples
    --------
    >>> from detect_peaks import detect_peaks
    >>> x = np.random.randn(100)
    >>> x[60:81] = np.nan
    >>> # detect all peaks and plot data
    >>> ind = detect_peaks(x, show=True)
    >>> print(ind)

    >>> x = np.sin(2*np.pi*5*np.linspace(0, 1, 200)) + np.random.randn(200)/5
    >>> # set minimum peak height = 0 and minimum peak distance = 20
    >>> detect_peaks(x, mph=0, mpd=20, show=True)

    >>> x = [0, 1, 0, 2, 0, 3, 0, 2, 0, 1, 0]
    >>> # set minimum peak distance = 2
    >>> detect_peaks(x, mpd=2, show=True)

    >>> x = np.sin(2*np.pi*5*np.linspace(0, 1, 200)) + np.random.randn(200)/5
    >>> # detection of valleys instead of peaks
    >>> detect_peaks(x, mph=0, mpd=20, valley=True, show=True)

    >>> x = [0, 1, 1, 0, 1, 1, 0]
    >>> # detect both edges
    >>> detect_peaks(x, edge='both', show=True)

    >>> x = [-2, 1, -2, 2, 1, 1, 3, 0]
    >>> # set threshold = 2
    >>> detect_peaks(x, threshold = 2, show=True)
    """

    x = np.atleast_1d(x).astype('float64')
    if x.size < 3:
        return np.array([], dtype=int)
    if valley:
        x = -x
    # find indices of all peaks
    dx = x[1:] - x[:-1]
    # handle NaN's
    indnan = np.where(np.isnan(x))[0]
    if indnan.size:
        x[indnan] = np.inf
        dx[np.where(np.isnan(dx))[0]] = np.inf
    ine, ire, ife = np.array([[], [], []], dtype=int)
    if not edge:
        ine = np.where((np.hstack((dx, 0)) < 0) & (np.hstack((0, dx)) > 0))[0]
    else:
        if edge.lower() in ['rising', 'both']:
            ire = np.where((np.hstack((dx, 0)) <= 0) & (np.hstack((0, dx)) > 0))[0]
        if edge.lower() in ['falling', 'both']:
            ife = np.where((np.hstack((dx, 0)) < 0) & (np.hstack((0, dx)) >= 0))[0]
    ind = np.unique(np.hstack((ine, ire, ife)))
    # handle NaN's
    if ind.size and indnan.size:
        # NaN's and values close to NaN's cannot be peaks
        ind = ind[np.in1d(ind, np.unique(np.hstack((indnan, indnan-1, indnan+1))), invert=True)]
    # first and last values of x cannot be peaks
    if ind.size and ind[0] == 0:
        ind = ind[1:]
    if ind.size and ind[-1] == x.size-1:
        ind = ind[:-1]
    # remove peaks < minimum peak height
    if ind.size and mph is not None:
        ind = ind[x[ind] >= mph]
    # remove peaks - neighbors < threshold
    if ind.size and threshold > 0:
        dx = np.min(np.vstack([x[ind]-x[ind-1], x[ind]-x[ind+1]]), axis=0)
        ind = np.delete(ind, np.where(dx < threshold)[0])
    # detect small peaks closer than minimum peak distance
    if ind.size and mpd > 1:
        ind = ind[np.argsort(x[ind])][::-1]  # sort ind by peak height
        idel = np.zeros(ind.size, dtype=bool)
        for i in range(ind.size):
            if not idel[i]:
                # keep peaks with the same height if kpsh is True
                idel = idel | (ind >= ind[i] - mpd) & (ind <= ind[i] + mpd) \
                    & (x[ind[i]] > x[ind] if kpsh else True)
                idel[i] = 0  # Keep current peak
        # remove the small peaks and sort back the indices by their occurrence
        ind = np.sort(ind[~idel])

    if show:
        if indnan.size:
            x[indnan] = np.nan
        if valley:
            x = -x
        _plot(x, mph, mpd, threshold, edge, valley, ax, ind)

    return ind


def _plot(x, mph, mpd, threshold, edge, valley, ax, ind):
    """Plot results of the detect_peaks function, see its help."""
    try:
        import matplotlib.pyplot as plt
    except ImportError:
        print('matplotlib is not available.')
    else:
        if ax is None:
            _, ax = plt.subplots(1, 1, figsize=(8, 4))

        ax.plot(x, 'b', lw=1)
        if ind.size:
            label = 'valley' if valley else 'peak'
            label = label + 's' if ind.size > 1 else label
            ax.plot(ind, x[ind], '+', mfc=None, mec='r', mew=2, ms=8,
                    label='%d %s' % (ind.size, label))
            ax.legend(loc='best', framealpha=.5, numpoints=1)
        ax.set_xlim(-.02*x.size, x.size*1.02-1)
        ymin, ymax = x[np.isfinite(x)].min(), x[np.isfinite(x)].max()
        yrange = ymax - ymin if ymax > ymin else 1
        ax.set_ylim(ymin - 0.1*yrange, ymax + 0.1*yrange)
        ax.set_xlabel('Data #', fontsize=14)
        ax.set_ylabel('Amplitude', fontsize=14)
        mode = 'Valley detection' if valley else 'Peak detection'
        ax.set_title("%s (mph=%s, mpd=%d, threshold=%s, edge='%s')"
                     % (mode, str(mph), mpd, str(threshold), edge))
        # plt.grid()
        plt.show()

def arb_multimodal_fit(data, autosmooth=True):
    """Attempts to detect peaks in data, and fit an apropriately chosen sum of gaussians to it.
    Very much a work in progress.

    Paramaters
    ---------
    data : a 2d array, 2 cols, N rows. data[:,0] contains x values; data[:,1] contains y values.
    autosmooth : if True, first filters data through a Savitzsky-Golay filter with reasonable prechosen paramaters

    Returns
    -------
    An ordered triple containing:
    n : the number of gaussians fitted
    params : the paramaters of the fit
    cov : covariance matrix

    Example
    -------
    >>> import numpy as np
    >>> import matplotlib.pyplot as plt
    >>> bimodal = n_modal(2)
    >>> xx = np.arange(0, 10, 0.001)
    >>> yy_noisy = bimodal(xx, 3, 1, 10, 8, 0.2, 3) + np.random.normal(1, 1, 1000)
    >>> A = np.vstack((xx, yy_noisy)).T
    >>> n, params, cov = arb_multimodal_fit(A, autosmooth=False)
    >>> sigma = np.sqrt(np.diag(cov))
    >>> plt.plot(xx, yy_noisy, xx, n_modal(n)(xx, *params)

    """
    if autosmooth:
        data[:,1] = savitzky_golay(data[:,1], len(data[:,1]) // 20, 4)

    est_peaks = 2
    est_mode_indices = detect_peaks(data[:,1], mpd=len(data[:,1])//est_peaks)
    n = len(est_mode_indices)

    peaks = np.vstack(([[data[i,0], data[i,1]] for i in est_mode_indices]))
    expected_params = tuple(chain(*[(peaks[i,0], 1, peaks[i,1]) for i in range(len(peaks[:,0]))]))
    model = n_modal(len(peaks[:,0]))

    params, cov = curve_fit(model, data[:,0], data[:,1], expected_params)

    return n, params, cov

