import visa

import pickle

from .Patterns import PLdata

class Experiment:
    def __init__(self, rm):
        self.devices = {}
        self.rm = rm

    def add_connection(self, name, addr, t):
        self.devices[name] = t(addr, rm)

    def pump(self, **kwargs):
        laser_energy_reps = 'Po Im Io'.split()
        if not any([rep in kwargs.keys() for rep in laser_energy_reps]):
            raise ValueError("In order to pump sample, the system needs to know how much energy. Add one of Po, Im, Io as kwargs with an appropriate value in mW or mA.")
        if 't' not in kwargs.keys():
            raise ValueError("In order to pump sample, the system needs to know how much time. Please add t with an appropriate value in seconds to kwargs for this function.")
        if 'LDD' in kwargs.keys():
            ldd = kwargs['LDD']
        elif 'LDD' in self.devices.keys():
            ldd = self.devices['LDD']
        else:
            raise ValueError("No LDD set in expeiment configuration or function call.")

        ldd.timed_pump(t)

    def image(self):
        pass
        #TODO:Implement
        #returns image data as numpy matrix, column 1 is wavelengths, column 2 is intensities


    def save_data(self, filepath, data):
        "Use to serialize a PLdata object as pickle to location filepath."
        with open(filepath, 'w+') as handle:
            pickle.dump(data, handle)


