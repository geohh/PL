#Provides nice, object-oriented controllers for CCD like JY-H Spectrum ONE.

import visa
import time

from libPL.Patterns import AbstractGPIBDevice
from .Utils import *

class AbstractCCD(AbstractGPIBDevice):
    pass

class SpectrumONE(AbstractCCD):
    pretty_name = "Spectrum ONE 3000"
    default_timeout = 500

    def __init__(self, address, rm):
        self.rm = rm

        # Options: automatic writeline ends are removed
        # because as far as I can tell, all of JY-H's
        # IEEE488 commands make no f*cking sense
        # and end with \r only if they take an argument (sometimes).
        self.resource=rm.open_resource(address, write_termination='', send_end=False)

        self.instrument_address = address

        #information on the ROM tables
        self.table_path = 'data/'
        self.tables     = {\
                        'stidle':   (0x0000, 0),\
                        'serwconv': (0x0400, 0),\
                        'serclear': (0x0800, 0),\
                        'serbin':   (0x0c00, 0),\
                        'partrans': (0x1000, 0),\
                        'bconvert': (0x1400, 0),\
                        'econvert': (0x1800, 0),\
                        'nidle':    (0x1c00, 0),\
                        }

    def reboot_if_hung(self):
        self.resource.write_binary_values('',[222])

    def get_status(self):
        return self.resource.query(" ")

    @property
    def firmware_version(self):
        response = self.resource.query("z")
        if response[0] != 'o':
            raise MonochromatorUnexpectedStatusReturnException("Failed to read firmware version: instead got: {0}".format(response))
        return float(response[2:6])



    def load_all_rom_tables(self):
        for table in self.tables.keys():
            self.load_rom_table(table)

    def ccd_init(self):
        r = self.resource.query("Z300,0\r")
        if r == "o0\r":
            raise CCDHardwareError("CCD hardware not detected when initialiazing.")
        elif r != "o1\r":
            raise CCDHardwareError("CCD_INIT returned unexpected exit status {0}.".format(r))

    # Hard-coded to our hardware
    def write_chip_stuff_literal(self):
        command = "Z328,0,0,512,1,2,0,0,0,5,0,300,3,400000000,0,4,500,500,1,513"
        status = self.resource.query(command+"\r")
        assert status == "o"


    def setup(self):
        status = self.get_status()
        while status != "F":
            if status == "B":
                self.resource.query("O2000\0\r")
            else:
                self.reboot_if_hung()
            time.sleep(5000)

        self.ccd_init()
        self.load_all_rom_tables()

    def read_temperature(self):
        ccd_temp_r = self.query("Z345,0,C9\r")
        analog_ground_r = self.query("Z345,0,CF\r")
        adc_ref_r = self.query("Z345,0,CB\r")

        ccd_temp_v = float(ccd_temp_r[1:-1])
        analog_ground_v = float(analog_ground_r[1:-1])
        adc_ref_v = float(adc_ref_r[1:-1])

        return (ccd_temp_v - analog_ground_v) * 3000 / (adc_ref_v - analog_ground_v)



class CCDHardwareError(Exception):
    pass

