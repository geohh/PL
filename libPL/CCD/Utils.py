#! pipenv run -- python

#This file defines various utility functions related to CCD operation
#Note: calls eval()! Do not run on unsanitied input!

import numpy as np

def readtab(filename):
    tabfile = open(filename)

    c = [[],[],[],[]]
    for line in tabfile.read().split('\n'):
        if line == '':
            break
        for i in range(4):
            c[i].append(eval('0x'+line[2*i:2*i+1]))

    return c
