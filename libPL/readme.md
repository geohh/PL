libPL -- a library of scripts and abstractions for PL
---

libPL.LDD, libPL.CCD, libPL.Monochromator declare software abstractions for physical devices.
libPL.Analysis provides a set of scripts commonly used in analysis of PL data.
libPL.Experiment provides an abstraction for bringing it all together as a cohesive experiment interface.



