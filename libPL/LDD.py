import visa
import time

from .Patterns import AbstractGPIBDevice

class AbstractLDD(AbstractGPIBDevice):

    def __init__(self, *args):
        raise NotImplementedError("Abstract LDD cannot be instantiated.")

    ### Minimum necessary fields/driver methods: just on/off via self.on
    @property
    def on(self):
        raise NotImplementedError()

    @on.setter
    def on(self, v):
        raise NotImplementedError()



    ### Generic, high-level methods any LDD should have:

    def activate(self):
        self.on = True

    def deactivate(self):
        self.on = False

    def timed_pump(self, t): # t in ms
        self.on = True
        time.sleep(t)
        self.on = False




class Newport5005(AbstractLDD):

    def __init__(self, resource_id, rm):
        # super.__init__()
        # TODO: Make this work nicely


        self.resource = rm.open_resource(resource_id, write_termination=None)

        self.__power = 0.0 #units of mW
        self.__on = False
        self.resource.write("*RST;*CLS;*PSC 1")

        self.__current_limit = 55 # units of mA
        self.resource.write("LAS:LIM:LDI {0}".format(self.__current_limit))
        self.__voltage_limit = 1.5 # units of V
        self.resource.write("LAS:LIM:LDV {0}".format(self.__voltage_limit))
        self.__power_limit = 2 # units of mW
        self.resource.write("LAS:LIMIT:MDP {0}".format(self.__power_limit))
        self.__calibration_value = 113.00
        self.resource.write("LAS:CALMD {0}".format(self.__calibration_value))
        self.mode = "Po"
        self.laser_current = 0.00

        self.resource.write("TERM 3")
    # 5005 doesn't offer a way to check any settings that I've figured out,
    # so I just set everything at the beginning to 0 with *RST and save it in
    # a software-level class field from there.
    @property
    def power(self):
        return self.__power

    @power.setter
    def power(self, power):
        power = float(power)
        self.__power = power
        self.resource.write("LAS:MDP {0}".format(power))

    @property
    def laser_current(self):
        return self.__las_curr

    @laser_current.setter
    def laser_current(self, ldi):
        ldi = float(ldi)
        self.__las_curr = ldi
        self.resource.write("LAS:LDI {0}".format(ldi))

    @property
    def on(self):
        return self.__on

    @on.setter
    def on(self, on):
        if on == True:
            self.__on = True
            self.resource.write("LAS:OUT 1")
        elif on == False:
            self.__on = False
            self.resource.write("LAS:OUT 0")
        else:
            raise ValueError("Lasers can only be on or off, bucko.")

    @property
    def current_limit(self):
        return self.__current_limit

    @current_limit.setter
    def current_limit(self, current_limit):
        current_limit = int(current_limit)
        self.__current_limit = current_limit
        self.resource.write("LAS:LIM:LDI {0}".format(current_limit))

    @property
    def voltage_limit(self):
        return self.__voltage_limit

    @voltage_limit.setter
    def voltage_limit(self, lim):
        lim = float(lim)
        self.__voltage_limit = lim
        self.resource.write("LAS:LIM:LDV {0}".format(lim))

    @property
    def power_limit(self):
        return self.__power_limit

    @power_limit.setter
    def power_limit(self, lim):
        lim = float(lim)
        self.__power_limit = lim
        self.resource.write("LAS:LIM:MDP {0}".format(lim))

    @property
    def mode(self):
        return self.__mode

    @mode.setter
    def mode(self, mode):
        if mode == "Po":
            self.resource.write("LAS:MODE:MDP")
            self.__mode = "Po"
        elif mode == "Io":
            self.resource.write("LAS:MODE:LDI")
            self.__mode = "Io"
        else:
            raise ValueError("Modes other than \"power\" and \"current\" are not currently supported")

