import visa
import time
import numpy as np

from .Patterns import  AbstractGPIBDevice, Spex488Mixin

class AbstractMonochromator(AbstractGPIBDevice):
    pretty_name = "Abstract Spectrophotometer"

#    def __init__(self, *args, **kwargs):
#        raise NotImplementedError("Abstract spectrophotometer lacks an actual implementation, so it can't in instantiated.")



class Triax320(AbstractMonochromator, Spex488Mixin):
    default_timeout = 300
    pretty_name = "Triax 320"
    backlash = 400 #units of motor steps
    # Dict of turret gratings and their respective spectral lengths
    default_turret = {\
        0: {'grating': 600,\
                'blaze': 1000,\
                'offset': -931,\
                'a': [-4.0464087323e+1,\
                       9.3634810492e-1,\
                       5.9022984665e-5,\
                      -1.5203639409e-8,\
                          ],\
                'b': [ 2.0151114776e-1,\
                       1.2626759386e-4,\
                      -1.2374387802e-7,\
                       2.8590732941e-11,\
                     ],\
            },\
        1: {'grating': 300,\
                'blaze': 1000,\
                'offset': -957,\
                #TODO: Finish importing these
            },\
        2: {'grating': 150,\
                'blaze':1200,\
                'offset': -666,\
            },\
    }



    def __init__(self, address, rm):
        self.rm=rm

        # Options: automatic writeline ends are removed
        # because the TRIAX's IEEE488 commands make no f*cking sense
        # and end with \r only if they take an argument (sometimes).
        self.resource=rm.open_resource(address, write_termination='', send_end=False)

        self.instrument_address = address

        # Should be overridden at run-time if not using "default" turret setup
        self.turret = self.default_turret

        # Base grating, around which other calculations are based (from LabView drivers).
        self.base_grating = 1200

        # Steps/nm (from LabView Driver)
        self.steps_nm = 16




        # Raising some low_level GPIB interaction methods for ease of scripting
        self.query = self.resource.query
        self.write_raw = lambda r: self.resource.write_binary_values('',[r])

    def motor_init(self):
        self.resource.timeout = 20000
        status = self.resource.query('A')
        if status != 'o':
            raise Exception("MOTOR INIT returned an unexpected exit status {0} for monochromator {1} at {2}.\
                             Aborting.".format(status, type(self).pretty_name, self.instrument_address))
        self.resource.timeout = Triax320.default_timeout

    def setup(self):
        super().setup(self)
        self.motor_init()

   # Assumes default grating of 1200
    def get_focused_wavelength(self):
        r = self.resource.query("Z62,1\r")
        if r[0] != o:
            raise MonochromatorUnexpectedStatusReturnException("MW_X_READ_WORKING_ABS_POSITION returned an unexpected value: {0}".format(r))
        else:
            return float(r[1:-1])

    # Assumes default grating of 1200
    def set_focused_wavelength(self, wavelength):
        response = triax.query("Z60,1,{0}\r".format(wavelength))
        if response != 'o':
            raise MonochromatorUnexpectedStatusReturnException("MW_X_SET_WORKING_ABS_POSITION returned an unexpected value: {0}".format(response))


    # Assumes default grating of 1200
    def move_focused_wavelength(self, delta_wavelength):
        response = triax.query("Z61,1,{0}\r".format(delta_wavelength))
        if response != 'o':
            raise MonochromatorUnexpectedStatusReturnException("MW_X_MOVE_WORKING_ABS_POSITION returned an unexpected value: {0}".format(response))

    def motor_busy(self):
        response = self.query("E")
        if response[0] != 'o':
            raise MonochromatorUnexpectedStatusReturnException("MOTOR_BUSY_CHECK returned an unexpected value: {0}".format(response))
        if response[1] == 'q':
            return True
        if response[1] == 'z':
            return False
        raise MonochromatorUnexpectedStatusReturnException("MOTOR_BUSY_CHECK returned an unexpected value: {0}".format(response))

    @property
    def motor_location(self):
        s = self.resource.query("H0\r")
        if s[0] != 'o':
            raise MonochromatorUnexpectedStatusReturnException("MOTOR_READ_POSITION returned an unexpected value: {0}".format(s))
        return int(s[1:-1])

    @motor_location.setter
    def motor_location(self, motor_location):
        current_location = self.motor_location()
        self.resource.query("F0,{0}\r".format(motor_location - self.motor_location()))

        # Wait for move to finish before handing control back to script
        while self.motor_busy():
            time.sleep(100)



    def get_steps(self, wavelength, turret_pos):
        groove_density = self.turret[turret_pos]['grating']
        base_position = groove_density / self.base_grating * wavelength
        steps = base_position * steps_nm

        return int(steps)

    def move_to_wavelength(self, wavelength, turret_pos):
        target_steps = self.get_steps(wavelength, turret_pos)
        self.motor_location = target_steps



    # Frankly, I have no idea why this works.
    # I'm just copying the implementation from the old LabView code,
    # but it seems to return reasonable results.
    # -GHH
    def pixel_wavelengths(center_wavelength, turret, turret_pos, ccd_pixels):
        # Pull needed constants from turret information
        a = turret[turret_pos]['a']
        b = turret[turret_pos]['b']

        x1, x2 = 0, 0
        for i in range(4):
            x1 += a[i] * center_wavelength**i
            x2 += b[i] * center_wavelength**i

        pix_lambda = [(j+1)*x2 + x1 for j in range(ccd_pixels)]

        return np.array(pix_lambda)

    ###Turret control

    @property
    def turret_pos(self):
        response = self.resource.query("Z452,0,0,0\r")
        if response[0] != 'o':
            raise MonochromatorUnexpectedStatusReturnException("MW_X_READ_INDEX_DEVICE_POS returned unexcepted status {0} .".format(response))
        return int(response[1])

    @turret_pos.setter
    def turret_pos(self, turret_pos):
        response = self.resource.query("Z451,0,0,0,{0}\r".format(turret_pos))
        if response != 'o':
            raise MonochromatorUnexceptedStatusReturnException("MW_X_SET_INDEX_DEVICE_POS returned unexpected status {0} .".format(response))
        # Since I can't seem to make polling for turret motor status work,
        # adding a sleep here for the normal amount of time
        time.sleep(20000)



class MonochromatorUnexpectedStatusReturnException(BaseException):
    pass

class MonochromatorMotorOutOfBoundsException(BaseException):
    pass

